package net.project_ile.weathermonitor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DisplayActivity extends FragmentActivity implements DownloadCallback {

    private boolean mDownloading = false;
    private NetworkFragment mNetworkFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create new view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        // Get Intent
        Intent intent = getIntent();
        String provinceName = intent.getStringExtra(MainActivity.DISPLAY_INFO_MESSAGE);
        // Get province name from intent
        mNetworkFragment = NetworkFragment.getInstance(this.getSupportFragmentManager(), "http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mDownloading && mNetworkFragment != null) {
            // Execute the async download.
            mNetworkFragment.startDownload();
            mDownloading = true;
        }
    }

    @Override
    public void updateFromDownload(Object result) {
        TextView textView = (TextView)findViewById(R.id.txtView);
        if(result == null) {
            textView.setText("@string/loading_error");
        } else {
            textView.setText(result.toString());
        }
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    protected void showDialog(Context ct, String title, String message, String btnMessage)
    {
        new AlertDialog.Builder(ct)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(btnMessage,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
    }
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        ProgressBar progressBar;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        switch(progressCode) {
            case Progress.ERROR:
                showDialog(this, "@string/local_error_title", "@string/load_error", "@string/load_error_ok");
                break;
            case Progress.CONNECT_SUCCESS:
                // TODO
                progressBar.setProgress(0);
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                // TODO
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                progressBar.setProgress(progressBar.getMax() / 100 * percentComplete);
                // TODO
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                // TODO
                progressBar.setProgress(progressBar.getMax());
                break;
        }
    }

    @Override
    public void finishDownloading() {
        mDownloading = false;
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
    }
}
