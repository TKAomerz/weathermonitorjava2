package net.project_ile.weathermonitor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    //Intent to call DisplayActivity
    public static final String DISPLAY_INFO_MESSAGE = "net.project_ile.weathermonitor.PROVINCESELECTED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onBtnDisplayInformationClicked(View view) {
        // new intent to call DisplayActivity
        Intent displayIntent = new Intent(this, DisplayActivity.class);
        // Get selected province from radiogroup
        //      get radio group first
        RadioGroup rg = (RadioGroup)findViewById(R.id.rdgProvince);
        //      get selected radio button
        RadioButton rb = (RadioButton) findViewById(rg.getCheckedRadioButtonId());
        //      get province name
        String provinceName = rb.getText().toString();
        // Pass province name together with intent
        displayIntent.putExtra(DISPLAY_INFO_MESSAGE, provinceName);
        // Start intent
        startActivity(displayIntent);
    }
}
